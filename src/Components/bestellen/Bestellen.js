import Header from '../Header';
import Main from './Main'; 
import Footer from '../Footer';

function Bestellen() {
  return (
    <div className="Bestellen">
        <Header />
        <Main />
        <Footer />
    </div>

  );
}

export default Bestellen;
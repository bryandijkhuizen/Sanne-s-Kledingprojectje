import { Link } from 'react-router-dom'; 
import HeroBackground from '../Assets/Web 1920 – 1.png';
import Logo from '../Assets/Logo.svg';

function Header () {


  return (
    <div>
        
  <nav class="w-full bg-white md:pt-0 px-6 shadow-lg relative z-20 border-t border-b border-gray-400">
			<div class="container mx-auto max-w-4xl md:flex justify-between items-center text-sm md:text-md md:justify-start">
				<div class="w-full md:w-1/2 text-center md:text-left py-4 flex flex-wrap justify-center items-stretch md:justify-start md:items-start">
				<a href="/" ><img class="max-h-8" src={Logo}></img></a>

				</div>

			</div>
		</nav>
	
		<div class="w-full py-24 px-6 bg-cover bg-no-repeat bg-center relative z-10" style={{backgroundImage: 'url("https://images.unsplash.com/photo-1577733966973-d680bffd2e80?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80")'}}>
		
			<div class="container max-w-4xl mx-auto text-center">
				<h1 class="text-xl leading-tight md:text-3xl text-center text-gray-100 ">RedoClothing</h1>

				<a href="/about" class="mt-4 inline-block bg-white text-black no-underline px-4 py-3 shadow-lg">Over Ons</a>
			</div>

		</div>
    </div>
  );
}

export default Header;
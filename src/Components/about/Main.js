
function Main() {
  return (
    <div className="Main">
        <div class="w-full px-6 py-12 bg-white">
			<div class="container max-w-4xl mx-auto text-center pb-10">
				<h1 class="bg-black text-white px-4 py-3 no-underline">Over Ons</h1>
				<p class="text-lg font-light leading-relaxed mt-0 mb-4 text-blueGray-800 mt-4">
					Wij zijn drie jonge ondernemers en samen vormen wij <b>RedoClothing</b> en wij maken tassen van tweedehands (spijker)stoffen. Wij doen dit om verspilling van kleding tegen te gaan.
					<br/>
					<br/>
					Tevens zouden wij het enorm op prijs stellen als er mensen zijn die oude kleding hebben die zij kunnen missen, wij kunnen dit namelijk altijd gebruiken, je kunt ons mailen op <a class="font-bold" href="mailto:clothingredo@gmail.com">clothingredo@gmail.com</a> 
				</p>
			</div>
		</div>
        
    </div>

  );
}

export default Main;
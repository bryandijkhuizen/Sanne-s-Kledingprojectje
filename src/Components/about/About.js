import Header from '../Header';
import Main from './Main'; 
import Footer from '../Footer';

function About() {
  return (
    <div className="About">
        <Header />
        <Main />
        <Footer />
    </div>

  );
}

export default About;
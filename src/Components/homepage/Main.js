
function Main() {
  return (
    <div className="Main">
        <div class="w-full px-6 py-12 bg-white">
			<div class="container max-w-4xl mx-auto text-center pb-10">


				
				<h1 class="bg-black text-white px-4 py-3 no-underline">Onze Producten</h1>

			</div>

			<div class="container max-w-4xl mx-auto text-center flex flex-wrap items-start md:flex-no-wrap">
				
				<div class="my-4 w-full md:w-1/2 flex flex-col items-center justify-center px-4">
					<img src="https://images.unsplash.com/photo-1617636521143-dc136ec026d4?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" class="w-full h-64 object-cover mb-6" />

					<h2 class="text-xl leading-tight mb-2">Handtas</h2>
					<p class="mt-3 mx-auto text-sm leading-normal">De tas is ongeveer 30 centimeter breed en 40 centimeter hoog en van spijkerstof en kan in de wasmachine.</p>
						<p class="mt-3 mx-auto text-sm leading-normal font-bold">&#8364;14,99</p>
						<a href="/bestellen" class="bg-blue-900 text-white px-4 py-3 no-underline mt-4">Bestellen</a>

				</div>
				
				<div class="my-4 w-full md:w-1/2 flex flex-col items-center justify-center px-4">
					<img src="https://images.unsplash.com/photo-1617636521236-db69104aa220?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" class="w-full h-64 object-cover mb-6" />

					<h2 class="text-xl leading-tight mb-2">Mini Handtas</h2>
					<p class="mt-3 mx-auto text-sm leading-normal">De mini handtas is 30 centimeter breed en 20 centimeter hoog en kan in de wasmachine</p>
					<p class="mt-3 mx-auto text-sm leading-normal font-bold">&#8364;8,20</p>

						<a href="/bestellen" class="bg-blue-900 text-white px-4 py-3 no-underline mt-4">Bestellen</a>
						
				</div>

			</div>

		</div>
        
    </div>

  );
}

export default Main;
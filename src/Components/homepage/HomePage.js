import Header from '../Header';
import Main from './Main'; 
import Footer from '../Footer';

function HomePage() {
  return (
    <div className="HomePage">
        <Header />
        <Main />
        <Footer />
    </div>

  );
}

export default HomePage;
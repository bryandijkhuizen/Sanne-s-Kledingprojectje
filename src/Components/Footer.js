import { Link } from 'react-router-dom'; 


function Footer () {
  return (
    <div>
     <footer class="w-full bg-white px-6 border-t">
			<div class="container mx-auto max-w-4xl py-6 flex flex-wrap md:flex-no-wrap justify-between items-center text-sm">
				&copy;2021 RedoClothing. All rights reserved.
				<div class="pt-4 md:p-0 text-center md:text-right text-xs">
				<a href="/about" class="text-black no-underline hover:underline mr-5">Voor Contact: <a class="font-bold" href="mailto:clothingredo@gmail.com">clothingredo@gmail.com</a></a>

					<a href="/about" class="text-black no-underline hover:underline mr-5">Over Ons</a>
					<a href="https://instagram.com/redoclothing_" class="text-black no-underline hover:underline">Instagram</a>

				</div>
			</div>
		</footer>
    </div>
  );
}

export default Footer;
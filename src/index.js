import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import ScrollToTop from './Components/functions/ScrollToTop';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import HomePage from './Components/homepage/HomePage';
import Bestellen from './Components/bestellen/Bestellen';
import About from './Components/about/About';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <ScrollToTop>
       <Switch>
          <Route exact path="/" component={HomePage}/>
          <Route exact path="/bestellen" component={Bestellen}/>
          <Route exact path="/about" component={About} />
	    </Switch>
      </ScrollToTop>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
